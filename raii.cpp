//============================================================================
// Name        : raii.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include "LogWriter.h"

using namespace std;

int main() {
	LogWriter logWriter(logWriter.Info);
	logWriter.logMessage("This shouldn't be written.\n", logWriter.Debug);
	logWriter.changeToConsole();
	logWriter.logMessage("This warning should be written.\n", logWriter.Warning);
	logWriter.changeToFile();
	logWriter.logMessage("This info should be written.\n", logWriter.Info);
	return 0;
}
