/*
 * LogWriter.h
 *
 *  Created on: 12.04.2017
 *      Author: RENT
 */

#ifndef LOGWRITER_H_
#define LOGWRITER_H_

#include <iostream>
#include <fstream>
#include <string>


class LogWriter {
public:
	enum LogType {Fatal, Error, Warning, Info, Debug, Trace};
	LogWriter();
	LogWriter(LogType logLevel);
	~LogWriter();
	void logMessage(std::string message, LogType messageType);
	void setLoggingLevel(LogType logLevel){this->logLevel = logLevel;}
	void changeToFile();
	void changeToConsole();
private:
	std::ostream* output;
	std::ostream o;
	std::fstream f;
	LogType logLevel;
};

#endif /* LOGWRITER_H_ */
