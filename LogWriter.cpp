/*
 * LogWriter.cpp
 *
 *  Created on: 12.04.2017
 *      Author: RENT
 */

#include "LogWriter.h"
#include <ctime>

LogWriter::LogWriter(): o(std::cout.rdbuf()) {
	logLevel = Error;
	f.open("firstLog.log", std::ios_base::app);
	output = &f;
}
LogWriter::LogWriter(LogType logLevel): o(std::cout.rdbuf()) {
	this->logLevel = logLevel;
	f.open("firstLog.log", std::ios_base::app);
	output = &f;
}
LogWriter::~LogWriter() {
	f.close();
}
void LogWriter::logMessage(std::string message, LogType messageType) {
	std::string codeArray[6] = { "Fatal", "Error", "Warning", "Info", "Debug",
			"Trace" };
	std::time_t actualTime = std::time(0);
	if (messageType <= logLevel) {
		*output << std::ctime(&actualTime) << codeArray[messageType] << ": "
				<< message;
	}
}
void LogWriter::changeToFile() {
	output = &f;
}
void LogWriter::changeToConsole() {
	output = &o;
}
